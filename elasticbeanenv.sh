#!/bin/bash
#elasticbeanenvironments.sh

set -e

#Describe the elastic beanstalk environment
#Copy new version of environment from PROD to PCYA
#Create new application version in PCYA
#Update the environment with the new version

REGION=${REGION:-us-east-1}

Prod_Version=$(aws elasticbeanstalk describe-environments --environment-names Default-Environment --query Environments[*].VersionLabel --output text)

Pcya_Version=$(aws elasticbeanstalk describe-environments --environment-names Default-Environment-2 --query Environments[*].VersionLabel --output text)

S3_Prod="elasticbeanstalk-eu-west-1-706665555773"

S3_ProdKey="api-jva-client-scoring"

S3_Pcya="elasticbeanstalk-eu-west-1-706665555773"

S3_PcyaKey="api-jva-client-scoring"

if [ "$Prod_Version" = "$Pcya_Version" ];then

echo "Environment is up to date"

exit

else

aws s3 cp $file s3://$S3_Prod/$S3_ProdKey s3://$S3_Pcya/$S3_PcyaKey

aws elasticbeanstalk create-application-version \
--application-name "api-java-client-scoring-pcya" \
--version-label "$Prod_Version" \
--source-bundle "{\"S3Bucket\":\"$S3_Pcya\",\"S3Key\":\"$S3PcyaKey\$Prod_Version\"}"

aws elasticbeanstalk update-environment \
--environment-name "api-jva-client-scoring-pcya" \
--version-label "$Prod_Version"

echo "updated Version"

exit

fi
